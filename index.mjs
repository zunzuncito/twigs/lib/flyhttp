
import http from 'http'
import qs from 'querystring'

import url from 'url'

import URLUtil from 'zunzun/flyutil/url.mjs'

async function match_routes(req, res, route, url_path, upi) {
  try {
    let route_match = false;
    let route_url_path = route.url_path;

    if (url_path.includes("/")) {
      const url_path_items = url_path.split("/");
      url_path_items.shift();
      url_path = "/"+url_path_items.join("/");
    } else {
      url_path = "/";
    }

    if (typeof upi === "number") {
      route_url_path = route_url_path[upi]
    }

    let regex_match = false;
    if (route_url_path instanceof RegExp) {
     // console.log("RegExp", route_url_path);
      regex_match = route_url_path.exec(url_path);
     // console.log("regex_match", regex_match);
    }

    if (
      ((
        regex_match &&
        regex_match.length > 0
      ) || (
        route_url_path === url_path
      )) &&
      route.method === req.method
    ) {

      req.url_path = url_path;
      if (regex_match) {
        regex_match.shift();
        req.regex_match = req.url_groups = req.url_path_matches = [...regex_match];
      }

      route_match = true;
      for (let cb of route.cbs) {
        await new Promise(async (resolve) => {
          try {
            if (cb.constructor.name === 'AsyncFunction') {
              await cb(req, res, resolve);
            } else {
              cb(req, res, resolve);
            }
          } catch (e) {
            console.error(e.stack);
          }

        });
      }
    }
    return route_match;
  } catch (e) {
    console.error(e.stack);
    return false;
  }
}

export default class FlyHTTP {
  constructor(addr, port, cfg) {
    this.addr = addr || "127.0.0.1";
    this.port = port || 8080;

    if (!cfg) cfg = {};
    const csp_cb = (nonce) => {
      const nonce_str = nonce ? ` 'nonce-${nonce}'` : '';
      return `default-src 'self'; script-src 'self'${nonce_str}; style-src 'self'${nonce_str}; object-src 'self'; base-uri 'self'; form-action 'self'; frame-ancestors 'self';`;
    }
    this.csp_header = cfg.csp_header || csp_cb;

    this.secure_headers = cfg.secure_headers || {
      'Content-Security-Policy': this.csp_header(),
      'X-Content-Type-Options': 'nosniff',
      'X-Frame-Options': 'DENY',
      'Strict-Transport-Security': 'max-age=31536000; includeSubDomains; preload',
      'Referrer-Policy': 'strict-origin-when-cross-origin'
    }

    this.routes = [];
    this.middlewares = [];

    const _this = this;

    this.next_route_id = 0;
    this.next_middleware_id = 0;

    this.request_payload_limit = cfg && cfg.request_payload_limit ? cfg.request_payload_limit : 1e6; // ~1.9MB

    _this.reqcb = async (req, res) => {
      try {
        console.debug(`\x1b[1m\x1b[42m> HTTP >> \x1b[0m\x1b[1m ${req.method} ${req.url}\x1b[0m`);
        const url_path = req.url.replace(/\?.*/g, '');


        if (
          req.method === "POST" ||
          req.method === "PUT" ||
          req.method === "DELETE"
        ) {
          req.params = await new Promise((fulfil) => {
            if (
              req.headers['content-type'] &&
              req.headers['content-type'].startsWith('multipart/form-data')
            ) {
              fulfil({});
            } else {
              let req_body = '';
              req.on('data', function (data) {
                req_body += data;

                // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
                if (req_body.length > _this.request_payload_limit){
                  res.writeHead(413, {
                    "Content-Type": "text/plain",
                    ..._this.secure_headers
                  });
                  res.end('Payload Too Large');
                }
              });

              req.on('end', function () {
                if (req.method === "POST") {
                  if (req.headers["content-type"] == 'application/json') {
                    fulfil(JSON.parse(req_body));
                  } else {
                    fulfil(qs.parse(req_body));
                  }
                } else {
                  fulfil(qs.parse(req_body));
                }
              });
            }
          });
        } else if (req.method === "GET") {
          req.params = url.parse(req.url, true).query
        }

        if (!req.params) req.params = {};

        for (let mw of _this.middlewares) {
          for (let cb of mw.cbs) {
            await new Promise(async (resolve) => {
              try {
                if (cb.constructor.name === 'AsyncFunction') {
                  await cb(req, res, resolve);
                } else {
                  cb(req, res, resolve);
                }
              } catch (e) {
                console.error(e.stack);
              }
            });
          }
        }

//          setTimeout(async () => {
        let route_match = false;

        for (let route of _this.routes) {

          if (typeof route.url_path === "string" || route.url_path instanceof RegExp) {
            route_match = await match_routes(req, res, route, url_path);
          } else if (Array.isArray(route.url_path)) {
            for (let u = 0; u < route.url_path.length; u++) {
              let upath = route.url_path[u];
              route_match = await match_routes(req, res, route, url_path, u);
            }
          } else {
            console.error(new Error("Unknown URL path type:", typeof route.url_path));
          }

        }

        if (!route_match) {
          res.writeHead(404, {
            "Content-Type": "text/plain",
            ..._this.secure_headers
          });
          res.end('Not Found');
        }

//          }, 1000);
      } catch (e) {
        console.error(e.stack);
        res.writeHead(500, {
          "Content-Type": "text/plain",
          ..._this.secure_headers
        });
        res.end("Internal Server Error");
      }

    }

    _this.server = http.createServer(_this.reqcb);

    console.log(`HTTP server listening on:  http://${_this.addr}:${_this.port}`);
    _this.server.listen(_this.port, _this.addr);
  }

  close() {
    this.server.close();
  }

  get_secure_headers(nonce) {
    const sec_head = { ...this.secure_headers }
    if (nonce) sec_head['Content-Security-Policy'] = this.csp_header(nonce);
    return sec_head;
  }

  add_route(route) {
    route.id = this.next_route_id;
    this.next_route_id++;
    this.routes.push(route);
    return route.id;
  }

  use() {
    const cbs = [...arguments]
    const mw_obj = {
      id: this.next_middleware_id,
      cbs: cbs
    }
    this.next_middleware_id++;
    this.middlewares.push(mw_obj);
    return mw_obj.id;
  }

  delete(url_path) {
    let cbs = [...arguments]
    cbs.shift();
    return this.add_route({
      method: "DELETE",
      url_path: url_path,
      cbs: cbs
    });
  }

  get(url_path) {
    let cbs = [...arguments]
    cbs.shift();
    return this.add_route({
      method: "GET",
      url_path: url_path,
      cbs: cbs
    });
  }

  head(url_path) {
    let cbs = [...arguments]
    cbs.shift();
    return this.add_route({
      method: "HEAD",
      url_path: url_path,
      cbs: cbs
    });
  }

  post(url_path) {
    let cbs = [...arguments]
    cbs.shift();
    return this.add_route({
      method: "POST",
      url_path: url_path,
      cbs: cbs
    });
  }

  put(url_path) {
    let cbs = [...arguments]
    cbs.shift();
    return this.add_route({
      method: "PUT",
      url_path: url_path,
      cbs: cbs
    });
  }

  clear(route_id) {
    for (let route of this.routes) {
      if (route.id === route_id) {
        this.routes.splice(this.routes.indexOf(route), 1);
      }
    }
  }
}
